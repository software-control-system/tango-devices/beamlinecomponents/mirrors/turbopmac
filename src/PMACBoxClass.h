//=============================================================================
//
// file :         PMACBoxClass.h
//
// description :  Include for the PMACBoxClass root class.
//                This class is the singleton class for
//                the PMACBox device class.
//                It contains all properties and methods which the 
//                PMACBox requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
// $Date: 2011-07-21 13:44:56 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Motion/TurboPMAC/src/PMACBoxClass.h,v $
// $Log: not supported by cvs2svn $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _PMACBOXCLASS_H
#define _PMACBOXCLASS_H

#include <tango.h>
#include <PMACBox.h>


namespace PMACBox_ns
{//=====================================
//	Define classes for attributes
//=====================================
class badCommandCounterAttrib: public Tango::Attr
{
public:
	badCommandCounterAttrib():Attr("badCommandCounter", Tango::DEV_LONG, Tango::READ) {};
	~badCommandCounterAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PMACBox *>(dev))->read_badCommandCounter(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PMACBox *>(dev))->is_badCommandCounter_allowed(ty);}
};

class oKCommandCounterAttrib: public Tango::Attr
{
public:
	oKCommandCounterAttrib():Attr("oKCommandCounter", Tango::DEV_LONG, Tango::READ) {};
	~oKCommandCounterAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PMACBox *>(dev))->read_oKCommandCounter(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PMACBox *>(dev))->is_oKCommandCounter_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class GetFirmwareRevisionCmd : public Tango::Command
{
public:
	GetFirmwareRevisionCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	GetFirmwareRevisionCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~GetFirmwareRevisionCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PMACBox *>(dev))->is_GetFirmwareRevision_allowed(any);}
};



class ExecLowLevelCmdClass : public Tango::Command
{
public:
	ExecLowLevelCmdClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ExecLowLevelCmdClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ExecLowLevelCmdClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PMACBox *>(dev))->is_ExecLowLevelCmd_allowed(any);}
};



class ResetClass : public Tango::Command
{
public:
	ResetClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ResetClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ResetClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PMACBox *>(dev))->is_Reset_allowed(any);}
};



//
// The PMACBoxClass singleton definition
//

class
#ifdef WIN32
	__declspec(dllexport)
#endif
	PMACBoxClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static PMACBoxClass *init(const char *);
	static PMACBoxClass *instance();
	~PMACBoxClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	PMACBoxClass(string &);
	static PMACBoxClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace PMACBox_ns

#endif // _PMACBOXCLASS_H
