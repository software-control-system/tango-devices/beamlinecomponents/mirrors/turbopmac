#ifndef _SPI_UTIL_H_
#define _SPI_UTIL_H_

//- no data : use example : 	post_msg(*this, MY_USER_MSG_WITHOUT_DATA, COMMAND_TIMEOUT_MS, false);
//- with data use example : 	post_msg(*this, MY_USER_MSG_WITH_DATA, COMMAND_TIMEOUT_MS, data, true);


#include <yat4tango/DeviceTask.h>
#include <yat4tango/ExceptionHelper.h>

//- post message, no data
void post_msg (yat4tango::DeviceTask &t, size_t msg_id, size_t timeout_ms, bool wait)
     throw (Tango::DevFailed);

template <typename T>
void post_msg (yat4tango::DeviceTask &t, size_t msg_id, size_t timeout_ms,T const &data, bool wait = false)
     throw (Tango::DevFailed)
{
	yat::Message *msg = NULL;
	try
	{
		msg = yat::Message::allocate (msg_id, DEFAULT_MSG_PRIORITY, wait);
		msg->attach_data (data);
	}
  catch (yat::Exception & ye)
  {
   THROW_YAT_TO_TANGO_EXCEPTION (ye);
  }
	catch (...)
	{
    THROW_DEVFAILED ("UNKNOWN_ERROR",
                     "(...) Exception caught trying to allocate a message",
                     "Util::post_msg");
	}

	try
	{
		if (wait)
			t.wait_msg_handled (msg, timeout_ms);
		else
			t.post (msg, timeout_ms);
	}
	catch (yat::Exception& ex)
	{
		THROW_YAT_TO_TANGO_EXCEPTION (ex);
	}
	catch (...)
	{
    THROW_DEVFAILED ("UNKNOWN_ERROR",
                     "(...) Exception caught trying to post a message",
                     "Util::post_msg");
	}
};

#endif
