//=============================================================================
// PMACProxy.h
//=============================================================================
// abstraction.......Delta Tau PMAC communication class
// class.............PMACProxy
// original author...JC d'apres projet ControlBoxV2 de N.Leclercq - SOLEIL
//=============================================================================

#ifndef _PMAC_PROXY_H_
#define _PMAC_PROXY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/threading/Task.h>
#include <yat4tango/ExceptionHelper.h>

// ============================================================================
// SHORTCUT TO THE <PMACProxy> SINGLETON
// ============================================================================
#define PMAC_PROXY pmac::PMACProxy::instance()

// ============================================================================
// CONSTs
// ============================================================================
#define kDEFAULT_HWSP_PERIOD            100
#define kDEFAULT_HWSP_STARTUP_TMO       3000
#define kDEFAULT_STATUS_EXPIRATION_TMO  4 * kDEFAULT_HWSP_PERIOD
//-----------------------------------------------------------------------------
#define kDEFAULT_WMH_TMO                975
//-----------------------------------------------------------------------------


namespace pmac
{

// ============================================================================
// CONSTs
// ============================================================================
  typedef enum
  {
    PMAC_SOCK_CREATE_ERROR = 0,
    PMAC_NOCONNECT_ERROR,
    PMAC_SOCK_CLOSE,
    PMAC_COM_ERROR_1,
    PMAC_COM_ERROR_2,
    PMAC_MSG_ERROR,
    PMAC_BUFFSIZE_ERROR,
    PMAC_WRITE_ERROR,
    PMAC_UNKNOWN_ERROR,
    PMAC_NO_ERROR
  } ComState;

  //- the status strings for ComState
  const size_t COM_STATE_MAX_SIZE = 10;
  static const std::string com_state_str[COM_STATE_MAX_SIZE] =
  {
    "Socket Creation failed",
    "could not connect Error",
    "socket is closed",
    "Communication Error 1",
    "Communication Error 2",
    "PMAC Message Error",
    "PMAC Buffer size Error",
    "Buffer Write Error",
    "Unknown Error",
    "Communication Running"
  };

// ============================================================================
// SHORTCUT TO THE <PMAC> SINGLETON
// ============================================================================
  #define PMAC_PROXY pmac::PMACProxy::instance()

// ============================================================================
// class: PMACProxy <SINGLETON>
// ============================================================================
class PMACProxy : public yat::Task
{

public:

  //- create a dedicated type for PMACProxy configuration
  //---------------------------------------------------------
  typedef struct Config
  {
    //- members --------------------
    std::string ip_addr;
    size_t port;
    size_t period_ms;
    size_t startup_timeout_ms;
    size_t status_expiration_timeout_ms;
    //- ctor -----------------------
    Config ();
    //- operator= ------------------
    void operator= (const Config& src);
  } Config;

  //----------------------------------------
  // SINGLETON ACCESSOR (SEE HW_STATUS MACRO)
  //----------------------------------------
  static inline PMACProxy * instance ()
		throw (Tango::DevFailed)
  {
    if (! PMACProxy::singleton)
        THROW_DEVFAILED(_CPTC("SOFTWARE_ERROR"),
                        _CPTC("unexpected NULL pointer [PMACProxy singleton not properly initialized]"),
                        _CPTC("PMACProxy::instance"));
    return PMACProxy::singleton;
  }

  //- The following members are thread safe
  //----------------------------------------

  //- singleton configuration --------------
  void configure (const Config& cfg)
		throw (Tango::DevFailed);


  //- Timestamp of the last 'full' status update
  inline void last_update_timestamp (yat::Timestamp& ts,
                                     double& elapsed_secs)
  {
    //- enter critical section
    yat::AutoMutex<> guard(this->Task::m_lock);
    //- get both timestamp and elapsed secs
	  yat::Timestamp now;
    _COPY_TIMESTAMP(this->m_last_update_ts, ts);
	  _GET_TIME(now);
	  elapsed_secs = _ELAPSED_SEC(this->m_last_update_ts, now);
  }

  //- Returns the "status expired" error counter
  inline unsigned long status_expired_error_counter (void)
  {
    unsigned long c = 0;
    {
      //- enter critical section
      yat::AutoMutex<> guard(this->Task::m_lock);
      // get value
      c = this->m_status_expired_error_counter;
    }
	  return c;
  }

  //- suspend the task activity
  void suspend_hw_status_update (void)
		throw (Tango::DevFailed);

  //- resume the task activity
  void resume_hw_status_update (void)
		throw (Tango::DevFailed);


  //- singleton instanciation --------------
  static void init ()
		throw (Tango::DevFailed);

  //- singleton release --------------------
  static void close ()
		throw (Tango::DevFailed);

  //- access to HW methods
  std::string get (std::string cmd)
    throw (Tango::DevFailed);

  //- access to HW methods
  std::string set (std::string cmd)
    throw (Tango::DevFailed);

  //- Communication State stuff ------------
  ComState get_com_state ( void)
  {
    return com_state;
  }
  //- Communication Status Accessor --------
  std::string get_com_status ( void)
  {
    return com_state_str [com_state];
  }
  unsigned long com_error (void)
  {
    return m_com_error;
  }
  unsigned long com_success (void)
  {
    return m_com_success;
  }

  //- force status update
  void force_status_update (void);


  //- Axis position accessor
  double axis_position (size_t axis_number)
    throw (Tango::DevFailed);
  //- Axis status   accessor
  void axis_status (size_t axis_number, std::string & resp)
    throw (Tango::DevFailed);
  //- Axis velocity accessor
  double axis_velocity (size_t axis_number)
    throw (Tango::DevFailed);

protected:
	//- handle_message -----------------------
	virtual void handle_message (yat::Message& msg)
		throw (yat::Exception);

private:

  //- The following members are NOT thread safe
  //-------------------------------------------

  //- the singleton ------------------------
  static PMACProxy * singleton;

	//- ctor ---------------------------------
	PMACProxy ();

	//- dtor ---------------------------------
	virtual ~PMACProxy ();

  //- update status in 1 shot
  void update_status (void);
  //- update positions in 1 shot
  void update_positions (void);
  //- update speed in 1 shot
  void update_velocities (void);

  //- write_read
  std::string write_read (std::string)
		throw (Tango::DevFailed);

  //- write
  void write (std::string)
	  throw (Tango::DevFailed);

  //- task configuration
  Config m_cfg;

  //- Elapsed time since last full status update
  yat::Timestamp m_last_update_ts;

  //- A counter for the so called "status expired error"
  unsigned long m_status_expired_error_counter;

  //- Flag: true if we obtained (at least) one full status
  bool m_updated_once;

  //- hw status update flag
  bool m_suspended;

  //- socket handle
  int sockfd;

  //- communication state
  ComState com_state;

  unsigned long m_com_error;
  unsigned long m_consecutive_comm_errors;
  unsigned long m_com_success;

  //- consecutive command errors (not enough data), RAZ on good response
  unsigned long m_command_errors;


  //- reads on TurboPMAC the 8 positions, status
  void read_hard (void);
  //- index for hardware polling
  size_t m_hw_poll_index;

  //- store locally the 8 positions read by read_hard
  double m_positions [8];
  //- store locally the 8 status read by read_hard
  std::string m_status [8];
  //- store locally the 8 velocity read by read_hard
  double m_velocities [8];

  //- check if following methods can return data
  inline void check_valid (size_t axis_number, std::string origin)
    throw (Tango::DevFailed);


  yat::Mutex com_mutex;
  yat::Mutex status_mutex;

  //- unimplemented/unsupported members
  //-----------------------------------
  PMACProxy (const PMACProxy &);
  PMACProxy & operator= (const PMACProxy &);
};

} // namespace pmac

#endif // _PMAC_PROXY_H_
