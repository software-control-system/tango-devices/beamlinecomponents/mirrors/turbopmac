
//- Project : PMAC motorisation
//- file : Axis.h
//- threaded reading of the HW


#ifndef __PMAC_AXIS_H__
#define __PMAC_AXIS_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>

namespace pmac_axis_ns
{
  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  //- startup and communication errors-------------------------------------------
  const size_t MAX_COM_RETRIES = 20;
  const size_t MAX_COMMAND_RETRIES = 5;


  typedef enum
  {
    PMAC_UNKNOWN_ERROR        = 0,
    PMAC_INITIALIZATION_ERROR,
    PMAC_COMMUNICATION_ERROR,
    PMAC_COMMAND_ERROR,
    PMAC_HARDWARE_ERROR,
    PMAC_SOFTWARE_ERROR,
    PMAC_NO_ERROR
  } PmacState;  


  //- the status strings for ComState
  const size_t PMAC_STATE_MAX_SIZE = 7;
  static const std::string pmac_state_str[PMAC_STATE_MAX_SIZE] =
  {
    "PMAC Unknown Error\n",
    "PMAC Initialisation Error\n",
    "PMAC Communication Error\n",
    "PMAC Command Error\n",
    "PMAC Hardware Error\n",
    "PMAC Software Error\n",
    "PMAC Communication Running\n"
  };

  typedef enum
  {
    AXIS_UNKNOWN_ERROR        = 0,
    AXIS_INITIALIZATION_ERROR,
    AXIS_FAULT,
    AXIS_ALARM,
    AXIS_STANDBY,
    AXIS_MOVING,
    AXIS_OFF,
    AXIS_ON
  } AxisState;  


  //- the status strings for ComState
  const size_t AXIS_STATE_MAX_SIZE = 8;
  static const std::string axis_state_str[AXIS_STATE_MAX_SIZE] =
  {
    "Axis Unknown Error\n",
    "Axis Initialisation Error\n",
    "Axis Fault\n",
    "Axis Alarm\n",
    "Axis Standby\n",
    "Axis Moving\n",
    "Axis OFF\n",
    "Axis ON\n"
  };
  
  
  //- state and status bits and strings
  typedef enum
  {
    PMAC_MOTOR_ACTIVATED = 0,
    PMAC_MOTOR_NOT_ACTIVATED,
    PMAC_NEG_END_LIM,
    PMAC_POS_END_LIM,
    PMAC_AMP_ENABLED,
    PMAC_OPEN_LOOP_MODE,
    PMAC_DESIRED_VEL_ZERO,
    PMAC_MOTOR_MOVING,
    PMAC_PHASED_MOTOR,
    PMAC_STOPPED_ON_DESIRED_POSITION_LIM,
    PMAC_STOPPED_ON_POSITION_LIM,
    PMAC_HOME_COMPLETE,
    PMAC_HOMING_NOT_DONE,
    PMAC_I2T_AMP_FAULT,
    PMAC_AMP_FAULT_ERROR,
    PMAC_FOLLOW_ERROR,
    PMAC_IN_POSITION
  }PmacAxisStatus;
  const size_t PMAC_AXIS_STATUS_MAX_SIZE = 17;
  static const std::string pmac_axis_status_str[PMAC_AXIS_STATUS_MAX_SIZE] =
  {
    "Motor ON (Motor Activated)\n",
    "Motor OFF( NOT Motor Activated)\n",
    "Negative limit (hard or soft) tripped\n",
    "Positive limit (hard or soft) tripped\n",
    "Motor ON (Amplifier Enabled)\n",
    "Motor Disabled (Open Loop Mode)\n",
    "In Position (Desired Velocity Zero)\n",
    "Motor is Moving\n",
    "Phased Motor\n",
    "Setpoint Out Of Range (Stopped on Desired position limit)\n",
    "Overtravel (Stopped on position limit)\n",
    "Home Complete\n",
    "Axis Not initialized [do homing first]\n",
    "Integrated current fault occured\n",
    "Amplifier fault\n",
    "Fatal Following Error\n",
    "In Position\n"
  };


  // ========================================================
  //---------------------------------------------------------  
  //- the YAT user messages 
  const size_t SET_AXIS_POSITION_MSG            = yat::FIRST_USER_MSG + 1000;
  const size_t SET_AXIS_VELOCITY_MSG            = yat::FIRST_USER_MSG + 1001;
  const size_t SET_AXIS_OFFSET_MSG              = yat::FIRST_USER_MSG + 1002;
  const size_t SET_AXIS_ACCELERATION_TIME_MSG   = yat::FIRST_USER_MSG + 1003;
  const size_t SET_AXIS_S_CURVE_TIME_MSG        = yat::FIRST_USER_MSG + 1004;

  const size_t PMAC_AXIS_INITIALIZE    = yat::FIRST_USER_MSG + 1010;
  const size_t PMAC_AXIS_HOME_SEARCH   = yat::FIRST_USER_MSG + 1011;
  const size_t PMAC_AXIS_STOP          = yat::FIRST_USER_MSG + 1012;
  const size_t PMAC_AXIS_KILL          = yat::FIRST_USER_MSG + 1013;
  const size_t PMAC_AXIS_RESET         = yat::FIRST_USER_MSG + 1014;


  //- structures for use with messages service to write values in the BT500
  //- the structure to write a string 
  typedef struct LowLevelMsg
  {
    std::string cmd;
  }LowLevelMsg;


  //------------------------------------------------------------------------
  //- Axis Class
  //- read the HW 
  //------------------------------------------------------------------------
  class Axis : public yat4tango::DeviceTask
  {
    public :

  //- the configuration structure
  typedef struct Config
  {
    //- members
    std::string axis_name;
    size_t axis_number;
    double ratio;
    size_t startup_timeout_ms;
    size_t read_timeout_ms;
    size_t periodic_timeout_ms;

    //- Ctor -------------
    Config ();
    //- Ctor -------------
    Config ( const Config & _src);
    //- Ctor -------------
    Config (std::string url );

    //- operator = -------
    void operator = (const Config & src);
  }Config;
   
    //- Constructeur/destructeur
    Axis (Tango::DeviceImpl * _host_device,
             Config & conf);

    virtual ~Axis ();

    PmacState   get_com_state (void)  
      {return m_pmac_state; };

    std::string get_com_status (void) 
      { return pmac_state_str [m_pmac_state]; };

    //- returns Axis State  
    AxisState get_axis_state (void);

    void get_axis_status (std::string & status);

    std::string get_last_error (void) 
      { return last_error; };

    //- access to position
    double position (void);
    //- position mutator
    void position (double);

    //- access to velocity
    double velocity (void);

    //- access to axis acceleration time
    double accelerationTime (void);

    //- access to axis s-curve time
    double s_curveTime (void);

    //- access to offset
    double offset (void);

    void stop (void);


    //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
    std::string exec_low_level_command (std::string cmd);

  protected:
	  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	  virtual void process_message (yat::Message& msg)throw (Tango::DevFailed);

  private :

    //- get values on the hard
    void read_hardware (void);

    //- get the state of the motor
    void get_axis_state_i (void);

    //- force state update
    void force_state_update (void);
    //- The Timeout timer to force state to moving
    yat::Timeout expiration_timer;
  
    //- communication with Box
    template <typename T> void write_read (std::string cmd, T & value)
    throw (Tango::DevFailed);

    //- au cas ou....
    yat::Mutex m_lock;
    yat::Mutex m_status_lock;

    //- the configuration
    Config conf;

    //- the host device 
    Tango::DeviceImpl * host_dev;

    //- the periodic execution time
    size_t periodic_exec_ms;
 
    //- the state and status stuff
    //- communication state
    PmacState m_pmac_state;
    //- PMAC state
    AxisState m_axis_state;
    //- the human readable axis status 
    std::string m_axis_status;
    //- store the result of PMAC Axis status
    unsigned long m_pmac_axis_status[2];

    std::string last_error;

    double m_offset;

    double m_velocity;



  };
}//- namespace
#endif //- __PMAC_AXIS_H__
