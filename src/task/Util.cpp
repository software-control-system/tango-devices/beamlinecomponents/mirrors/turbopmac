#include "Util.h"
#include <yat4tango/ExceptionHelper.h>

//- post message, no data attached
void post_msg (yat4tango::DeviceTask &t, size_t msg_id, size_t timeout_ms, bool wait = false)
  throw (Tango::DevFailed)
{
	yat::Message* msg = 0;
	try
	{
		msg = yat::Message::allocate (msg_id, DEFAULT_MSG_PRIORITY, wait);
	}
	catch (yat::Exception& ex)
	{
		THROW_YAT_TO_TANGO_EXCEPTION (ex);
	}
  catch (...)
  {
    THROW_DEVFAILED ("OUT_OF_MEMORY",
                     "caught (...) trying to allocate a msg without data",
                     "Utils::post_msg");
  }
	try
	{
		if (wait)
			t.wait_msg_handled (msg, timeout_ms);
		else
			t.post (msg, timeout_ms);
	}
	catch (yat::Exception& ex)
	{
		THROW_YAT_TO_TANGO_EXCEPTION (ex);
	}
	catch (...)
	{
    THROW_DEVFAILED ("UNKNOWN_ERROR",
                     "(...) Exception caught trying to post a message",
                     "Util::post_msg");
	}
}
