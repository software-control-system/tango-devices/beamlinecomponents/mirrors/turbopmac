
//- Project : PMAC Motorisation
//- file : Axis.cpp

#include "Axis.h"
#include <math.h>
#include <iomanip>
#include "PMACProxy.h"
#include <yat/utils/XString.h>
#include <yat4tango/ExceptionHelper.h>


namespace pmac_axis_ns
{
  // ============================================================================
  // Some defines and constants
  // ============================================================================
  static const double __NAN__ = ::sqrt(-1.);


  // ============================================================================
  // Config::Config
  // ============================================================================
  Axis::Config::Config ()
  {
    axis_name           = "Not Initialised";
    axis_number         = 0;
    ratio               = 1.0;
    startup_timeout_ms  = 2000;
    read_timeout_ms     = 1000;
    periodic_timeout_ms = 200;
  }

  Axis::Config::Config (const Config & _src)
  {
    *this = _src;
  }
  // ============================================================================
  // Config::operator =
  // ============================================================================
  void Axis::Config::operator = (const Config & _src)
  {
    axis_name           = _src.axis_name;
    axis_number         = _src.axis_number;
    ratio               = _src.ratio;
    startup_timeout_ms  = _src.startup_timeout_ms;
    read_timeout_ms     = _src.read_timeout_ms;
    periodic_timeout_ms = _src.periodic_timeout_ms;
  }


  //-----------------------------------------------
  //- Ctor ----------------------------------------
  Axis::Axis (Tango::DeviceImpl * _host_device,
                    Config & _conf) :
              yat4tango::DeviceTask(_host_device),
              conf (_conf),
              host_dev (_host_device)
  {
    DEBUG_STREAM << "Axis::Axis <- " << std::endl;

    //- yat::Task configure optional msg handling
    this->enable_timeout_msg(true);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(this->conf.periodic_timeout_ms);
    this->m_offset   = 0.;
    this->m_velocity = __NAN__;
    this->m_axis_status.reserve (1024);

  }


  //-----------------------------------------------
  //- Dtor ----------------------------------------
  Axis::~Axis (void)
  {
    DEBUG_STREAM << "Axis::~Axis <- " << std::endl;

    DEBUG_STREAM << "Disconnecting from peer..." << std::endl;

  }

  //-----------------------------------------------
  //- the user core of the Task -------------------
  void Axis::process_message (yat::Message& msg) throw (Tango::DevFailed)
  {
    //- The DeviceTask's lock_ -------------

    DEBUG_STREAM << "Axis::handle_message::receiving msg " << msg.to_string() << std::endl;

    //- handle msg
    switch (msg.type())
    {
      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "Axis::handle_message::THREAD_INIT::thread is starting up" << std::endl;
        //- "initialization" code goes here
        last_error = "No Error\n";

        //- do not allow ratio to be null
        if (conf.ratio == 0.)
          conf.ratio = 1.;
        //- reserve largely enough place for status string
        m_axis_status.reserve (2048);

        this->read_hardware ();

        //- force state moving stuff
        expiration_timer.set_unit (yat::Timeout::TMO_UNIT_MSEC);
        expiration_timer.set_value (100.0);
        //- has run to have time elapsed and good state
        expiration_timer.enable ();

      }
      break;
      //- TASK_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "Axis::handle_message handling TASK_EXIT thread is quitting" << std::endl;

        //- "release" code goes here
      }
      break;
      //- TASK_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM << "Axis::handle_message handling TASK_PERIODIC msg " << std::endl;
        //- code relative to the task's periodic job goes here
        //- ask directly to HW
        this->read_hardware ();

      }
      break;

      //- TASK_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        ERROR_STREAM << "Axis::handle_message handling TASK_TIMEOUT msg" << std::endl;
        m_pmac_state = PMAC_SOFTWARE_ERROR;
        this->last_error = "Axis::handle_message received TASK_TIMEOUT msg\n";
      }
      break;

      //- USER_DEFINED_MSG ================
    case SET_AXIS_OFFSET_MSG: //- TODO
      {
        DEBUG_STREAM << "Axis::handle_message handling SET_AXIS_OFFSET_MSG msg" << std::endl;
        { //- enter critical section
          double * d = 0;
          msg.detach_data (d);
          if (d)
          {
            yat::AutoMutex<yat::Mutex> guard(this->m_lock);
            m_offset = *d;
            delete d;
            DEBUG_STREAM << "Axis::handle_message::SET_AXIS_OFFSET_MSG new offset = " << m_offset << std::endl;
          }
        }
      }
      break;

    case SET_AXIS_POSITION_MSG:
      {
        DEBUG_STREAM << "Axis::handle_message handling SET_AXIS_POSITION_MSG msg" << std::endl;
          double * d = 0;
          msg.detach_data (d);
          if (d)
          {
            this->position (*d);
          }
      }
      break;

    case SET_AXIS_VELOCITY_MSG:
      {
        DEBUG_STREAM << "Axis::handle_message handling SET_AXIS_VELOCITY_MSG msg" << std::endl;
        { //- enter critical section
          double * d = 0;
          msg.detach_data (d);
          if (d)
          {

            // TODO : Si ca ne fonctionne pas changer suivant TANGODEVIC-1518

            std::stringstream s;
            s << "i"
              << conf.axis_number
              << "22="  //- command number for Velocity
              << *d
              << std::ends;
            std::string resp;
            this->write_read (s.str (), resp);
            DEBUG_STREAM << "Axis::handle_message::SET_AXIS_VELOCITY_MSG sent cmd = " << s.str () << std::endl;
          }
        }
      }
      break;

    case SET_AXIS_ACCELERATION_TIME_MSG:
      {
        DEBUG_STREAM << "Axis::handle_message handling SET_AXIS_ACCELERATION_TIME_MSG msg" << std::endl;
        { //- enter critical section
          double * d = 0;
          msg.detach_data (d);
          if (d)
          {            std::stringstream s;
            s << "i"
              << conf.axis_number
              << "20="  //- command number for acceleration time
              << *d
              << std::ends;
            std::string resp;
            this->write_read (s.str (), resp);
            DEBUG_STREAM << "Axis::handle_message::SET_AXIS_ACCELERATION_TIME_MSG sent cmd = " << s.str () << std::endl;
          }
        }
      }
      break;

    case SET_AXIS_S_CURVE_TIME_MSG:
      {
        DEBUG_STREAM << "Axis::handle_message handling SET_AXIS_S_CURVE_TIME_MSG msg" << std::endl;
        { //- enter critical section
          double * d = 0;
          msg.detach_data (d);
          if (d)
          {            std::stringstream s;
            s << "i"
              << conf.axis_number
              << "21="  //- command number for acceleration time
              << *d
              << std::ends;
            std::string resp;
            this->write_read (s.str (), resp);
            DEBUG_STREAM << "Axis::handle_message::SET_AXIS_S_CURVE_TIME_MSG sent cmd = " << s.str () << std::endl;
          }
        }
      }
      break;

    case PMAC_AXIS_HOME_SEARCH:
      {
        DEBUG_STREAM << "Axis::handle_message handling PMAC_AXIS_HOME_SEARCH msg" << std::endl;
        std::stringstream s;
        s << "#"
          << conf.axis_number
          << "HMZ"
          << std::ends;
          std::string resp;
          this->write_read (s.str (), resp);
          DEBUG_STREAM << "Axis::handle_message::PMAC_AXIS_HOME_SEARCH sent cmd = " << s.str () << std::endl;
      }
      break;
    case PMAC_AXIS_RESET:
      {
        DEBUG_STREAM << "Axis::handle_message handling PMAC_AXIS_RESET msg" << std::endl;
        std::stringstream s;
        s << "#"
          << conf.axis_number
          << "$"
          << std::ends;
          std::string resp;
          this->write_read (s.str (), resp);
          DEBUG_STREAM << "Axis::handle_message::PMAC_AXIS_RESET sent cmd = " << s.str () << std::endl;
      }
      break;
    case PMAC_AXIS_STOP:
      {
        DEBUG_STREAM << "Axis::handle_message handling PMAC_AXIS_STOP msg" << std::endl;
        this->stop ();
      }
      break;

    default:
  		  ERROR_STREAM<< "SublimationManagerTask::handle_message::unhandled msg type received" << std::endl;
  		break;
    } //- switch (_msg.type())
  } //- Axis::process_message


  //-----------------------------------------------
  //- write_read
  //- sends a Low Level Command to the PMACProxy(do not check the syntax)  to the hard and gets response
  //-----------------------------------------------
  template <typename T> void Axis::write_read (std::string cmd, T & value)
    throw (Tango::DevFailed)
  {
    DEBUG_STREAM << "Axis::write_read for command [" << cmd << "\n]" << std::endl;
    std::string resp ;
    if (PMAC_PROXY)
    {
      resp = PMAC_PROXY->get (cmd);
      DEBUG_STREAM << "Axis::write_read cmd [" << cmd << "] response [" << resp << "]" << std::endl;
    }
    else
    {
      ERROR_STREAM << "Axis::write ERROR HW proxy thread not started " << std::endl;
      //- TODO : set the error
      return;
    }
    try
    {
      value = yat::XString <T>::to_num (resp);
    }
    catch (yat::Exception & ye)
    {
      ERROR_STREAM << "Axis::write_read cmd " << cmd
                   << " failed with response " << resp
                   << " [conversion to numeric failed]" << std::endl;
      THROW_YAT_TO_TANGO_EXCEPTION(ye);
    }
    catch (...)
    {
      THROW_DEVFAILED("WRONG_FORMAT", "unexpected response ", "Axis::write_read");
    }
  }


  //-----------------------------------------------
  //- read_hardware
  //- periodic readings
  //-----------------------------------------------
  void Axis::read_hardware (void)
  {
    DEBUG_STREAM << "Axis::read_hardware <-" << std::endl;
    //- get the HWProxy state (does communication works?)
    if (PMAC_PROXY->get_com_state () != pmac::PMAC_NO_ERROR)
      m_pmac_state = PMAC_COMMUNICATION_ERROR;
    else
      m_pmac_state = PMAC_NO_ERROR;
    //- read Axis "?" and store it in 2 Unsigned Long
    //- read velocity and check if axis moving
    this->get_axis_state_i ();
  }

  //-----------------------------------------------
  //- position accessor
  //-----------------------------------------------
  double Axis::position (void)
  {
    DEBUG_STREAM << "Axis::position::Read <-" << std::endl;
    if (m_pmac_state == PMAC_COMMUNICATION_ERROR)
      return __NAN__;

    double val = PMAC_PROXY->axis_position (conf.axis_number);
    val *= conf.ratio;
    val += m_offset;
    return val;
  }

  //-----------------------------------------------
  //- position Mutator
  //-----------------------------------------------
  void Axis::position (double pos)
  {
    //- start timer forcing MOVING State
    expiration_timer.restart ();
    std::cout << "Axis::position::Write <- for position <" << pos << ">" << std::endl;
    DEBUG_STREAM << "Axis::position::Write <- for position <" << pos << ">" << std::endl;
    std::stringstream s;
    std::string resp;

    if (this->m_axis_state == AXIS_MOVING)
    {
      ERROR_STREAM << "Axis::position::Write: rejected request to move an already moving axis" << std::endl;
      THROW_DEVFAILED("COMMAND_NOT_ALLOWED",
                      "rejected request to move an already moving axis",
                      "Axis::position::Write");
    }


    { //- TODO : enter critical section ??
       double hw_position = (pos - m_offset) / conf.ratio;
       s << "#"
         << conf.axis_number
         << "J="
         << std::fixed
         << std::setprecision (2)
         << hw_position
         << std::ends;      //- In Position
      if ((m_pmac_axis_status[1] & 0x000001) != 0)
      {
        // request for moving
        if (expiration_timer.time_to_expiration () > 0.)
        {
          m_axis_state = AXIS_MOVING;
          m_axis_status += "Status forced to MOVING\n" ;
        }
        m_axis_state = AXIS_STANDBY;
        m_axis_status += pmac_axis_status_str [PMAC_IN_POSITION];
      }

       DEBUG_STREAM << "Axis::handle_message::SET_AXIS_POSITION_MSG user position = "
                    << pos
                    << " HW position = "
                    << hw_position
                    << std::endl;

       { //- Critical section lock write position VS status
         //- yat::AutoMutex <yat::Mutex> lock (m_status_lock);
         this->write_read (s.str (), resp);
std::cout << "Axis::position::write cmd : [" << s.str () << "] response : [" << resp << "]" << std::endl;
       }
       //- force the PMACProxy Axis Status Update + force MOVING state
       this->force_state_update ();
       DEBUG_STREAM << "Axis::handle_message::SET_AXIS_POSITION_MSG sent cmd = " << s.str () << std::endl;

     }
  }


  //-----------------------------------------------
  //- velocity accessor
  //-----------------------------------------------
  double Axis::velocity (void)
  {
    DEBUG_STREAM << "Axis::velocity <-" << std::endl;

    if (m_pmac_state == PMAC_COMMUNICATION_ERROR)
      return __NAN__;

    double vel = PMAC_PROXY->axis_velocity (conf.axis_number);

    return vel;
  }

// FOR TESTS : JIRA TANGODEVIC-1518

  //-----------------------------------------------
  //- accelerationTime accessor
  //-----------------------------------------------
  double Axis::accelerationTime (void)
  {
    DEBUG_STREAM << "Axis::accelerationTime <-" << std::endl;
    if (m_pmac_state == PMAC_COMMUNICATION_ERROR)
      return __NAN__;

    short accTimeCommand = 20;
 //   double val = PMAC_PROXY->axis_accelerationTime (conf.axis_number);
    std::stringstream s;
    std::string resp;
    //- cmd format : i + axis number + command number
    s << "i" << conf.axis_number << accTimeCommand;
    this->write_read (s.str (), resp);
    double val = yat::XString <double>::to_num (resp);

    return val;
  }

  //-----------------------------------------------
  //- s_curveTime accessor
  //-----------------------------------------------
  double Axis::s_curveTime (void)
  {
    DEBUG_STREAM << "Axis::s-curveTime <-" << std::endl;
    if (m_pmac_state == PMAC_COMMUNICATION_ERROR)
      return __NAN__;

    short s_curveTimeCommand = 21;
 //   double vel = PMAC_PROXY->axis_velocity (conf.axis_number);
    std::stringstream s;
    std::string resp;
    //- cmd format : i + axis number + command number
    s << "i" << conf.axis_number << s_curveTimeCommand;
    this->write_read (s.str (), resp);
    double val = yat::XString <double>::to_num (resp);

    return val;
  }


  //-----------------------------------------------
  //- STOP
  //-----------------------------------------------
  void Axis::stop (void)
  {
    DEBUG_STREAM << "Axis::stop <-" << std::endl;

    std::stringstream s;
    std::string resp;
    s << "#" << conf.axis_number << "J/" << std::ends;
    this->write_read (s.str (), resp);
    INFO_STREAM << "Axis::stop cmd ["
                << s.str ()
                << "] returned ["
                << resp
                << "]"
                << std::endl;
  }

  //-----------------------------------------------
  //- offset accessor
  //-----------------------------------------------
  double Axis::offset (void)
  {
    DEBUG_STREAM << "Axis::offset <-" << std::endl;
    if (m_pmac_state == PMAC_COMMUNICATION_ERROR)
      return __NAN__;

    return m_offset;
  }

  //-----------------------------------------------
  //- get_axis_state_i refresh the motor state
  //- must be called periodically
  //-----------------------------------------------
  void Axis::get_axis_state_i (void)
  {
    DEBUG_STREAM << "Axis::get_axis_state_i <-" << std::endl;
    if (m_pmac_state == PMAC_COMMUNICATION_ERROR)
      return;
    //- the motor state
    std::string resp;
    resp.reserve (32);
    try
    {
      PMAC_PROXY->axis_status (conf.axis_number, resp);
    }
    catch (Tango::DevFailed &e)
    {
      std::stringstream err;
      err << "error : trying to get raw status caught DevFailed <"
          << e.errors[0].desc
          << "> got response ["
          << resp
          << "]" << std::endl;
      this->last_error = err.str ();
      ERROR_STREAM << "Axis::get_axis_state_i " << err.str ();
      m_pmac_state = PMAC_COMMAND_ERROR;
      return;
    }
    catch (...)
    {
      std::stringstream err;
      err << "error : trying to get raw status caught (...) got response ["
          << resp
          << "]" << std::endl;
      this->last_error = err.str ();
      ERROR_STREAM << "Axis::get_axis_state_i " << err.str ();
      m_pmac_state = PMAC_COMMAND_ERROR;
      return;
    }
    DEBUG_STREAM << "Axis::get_axis_state_i raw axis status = [" << resp << "]" << std::endl;
    //- store it in the m_motor_status
    if (resp.find ("ERR") != std::string::npos)
    {
      std::stringstream err;
      err << "error : trying to get raw status got response ["
          << resp
          << "]" << std::endl;
      this->last_error = err.str ();
      ERROR_STREAM << "Axis::get_axis_state_i " << err.str ();
      m_pmac_state = PMAC_COMMAND_ERROR;
      return;
    }

    if (resp.size () < 12)
    {
      std::stringstream err;
      err << "error : trying to get raw status got response ["
          << resp
          << "]" << std::endl;
      this->last_error = err.str ();
      ERROR_STREAM << "Axis::get_axis_state_i " << err.str ();
      m_pmac_state = PMAC_COMMAND_ERROR;
      return;
    }
    else
    {
      std::istringstream tmp1 (resp.substr (0,6));
      tmp1 >> std::hex >> m_pmac_axis_status[0];
      std::istringstream tmp2 (resp.substr (6));
      tmp2 >> std::hex >> m_pmac_axis_status[1];

    }

    { //- status into critical section
      yat::AutoMutex <yat::Mutex> guard (this->m_status_lock);
      m_axis_status.clear ();
      //- ordre de priorite de haut en bas (en bas les plus prioritaires)
      //- ON
      //- STANDBY
      //- ALARM
      //- OFF
      //- FAULT
      //- MOVING
      //- force MOVING state stuff
      if (expiration_timer.time_to_expiration () > 0.)
      {
        m_axis_state = AXIS_MOVING;
        return;
      }


      std::stringstream s;
      s << "Axis::get_axis_state_i status = ["
        << resp
        << "] substr (0,6) = ["
        << std::hex << m_pmac_axis_status[0]
        << "] substr (6,6) = ["
        << std::hex << m_pmac_axis_status[1] << "]"
        << std::endl;

        m_axis_status = s.str () ;

      //- MOTOR ON
      if (((m_pmac_axis_status[0] & 0x800000) != 0) && ((m_pmac_axis_status[0] & 0x040000) != 0))
      {
        m_axis_state = AXIS_ON;
        m_axis_status += pmac_axis_status_str [PMAC_MOTOR_ACTIVATED];
      }

      //- MOTOR ON
      if ((m_pmac_axis_status[0] & 0x080000) != 0)
      {
        m_axis_state = AXIS_ON;
        m_axis_status += pmac_axis_status_str [PMAC_AMP_ENABLED];
      }

      //- Motor Phased
      if ((m_pmac_axis_status[0] & 0x000040) != 0)
      {
        m_axis_status += pmac_axis_status_str [PMAC_PHASED_MOTOR];
      }

      //- LIMIT SWITCHS /Soft limits
      if ((m_pmac_axis_status[0] & 0x400000) != 0)
      {
        m_axis_state = AXIS_ALARM;
        m_axis_status += pmac_axis_status_str [PMAC_NEG_END_LIM];
      }
      if ((m_pmac_axis_status[0] & 0x200000) != 0)
      {
        m_axis_state = AXIS_ALARM;
        m_axis_status += pmac_axis_status_str [PMAC_POS_END_LIM];
      }

      //- stopped on desired limit
      if ((m_pmac_axis_status[1] & 0x001000) != 0)
      {
        m_axis_state = AXIS_ALARM;
        m_axis_status += pmac_axis_status_str [PMAC_STOPPED_ON_DESIRED_POSITION_LIM];
      }

      //- stopped on limit switch?
      if ((m_pmac_axis_status[1] & 0x000800) != 0)
      {
        m_axis_state = AXIS_ALARM;
        m_axis_status += pmac_axis_status_str [PMAC_STOPPED_ON_POSITION_LIM];
      }

      //- Homed?expiration_timer.time_to_expiration ()
      if ((m_pmac_axis_status[1] & 0x000400) != 0)
      {
        m_axis_status += pmac_axis_status_str [PMAC_HOME_COMPLETE];
      }

      else
      {
        m_axis_state = AXIS_ALARM;
        m_axis_status += pmac_axis_status_str [PMAC_HOMING_NOT_DONE];
      }
      //- MOTOR OFF
      if ((m_pmac_axis_status[0] & 0x800000) == 0)
      {
        m_axis_state = AXIS_OFF;
        m_axis_status += pmac_axis_status_str [PMAC_MOTOR_NOT_ACTIVATED];
      }
      if ((m_pmac_axis_status[0] & 0x040000) != 0)
      {
        m_axis_state = AXIS_OFF;
        m_axis_status += pmac_axis_status_str [PMAC_OPEN_LOOP_MODE];
      }

      //- I2T Amp Fault
      if ((m_pmac_axis_status[1] & 0x000020) != 0)
      {
        m_axis_state = AXIS_FAULT;
        m_axis_status += pmac_axis_status_str [PMAC_I2T_AMP_FAULT];
      }
      //- Amp Fault
      if ((m_pmac_axis_status[1] & 0x000008) != 0)
      {
        m_axis_state = AXIS_FAULT;
        m_axis_status += pmac_axis_status_str [PMAC_AMP_FAULT_ERROR];
      }
      //- Motor Stopped STANDBY
      if ((m_pmac_axis_status[0] & 0x002000) != 0)
      {
          // request for moving

        if (expiration_timer.time_to_expiration () > 0.)
        {
          m_axis_state = AXIS_MOVING;
          m_axis_status += "Status forced to MOVING\n" ;
        }
        m_axis_state = AXIS_STANDBY;
        m_axis_status += pmac_axis_status_str [PMAC_DESIRED_VEL_ZERO];
      }
            //- Motor MOVING
      if ((m_pmac_axis_status[0] & 0x002000) == 0)
      {
        m_axis_state = AXIS_MOVING;
        m_axis_status += pmac_axis_status_str [PMAC_MOTOR_MOVING];
      }
      //- In Position
      if ((m_pmac_axis_status[1] & 0x000001) != 0)
      {
        m_axis_state = AXIS_STANDBY;
        m_axis_status += pmac_axis_status_str [PMAC_IN_POSITION];
        // request for moving
        if (expiration_timer.time_to_expiration () > 0.)
        {
          m_axis_state = AXIS_MOVING;
          m_axis_status += "Status forced to MOVING\n" ;
        }
      }
      else
      {
        m_axis_state = AXIS_MOVING;
        m_axis_status += "Status forced to MOVING\n" ;
      }
    } //- end critical section
  }

 //-----------------------------------------------
  //- get_axis_state returns axis State
  //-----------------------------------------------
  AxisState Axis::get_axis_state (void)
  {
    { //- Critical section lock write position VS status
      //-yat::AutoMutex <yat::Mutex> lock (m_status_lock);
      DEBUG_STREAM << "Axis::get_axis_state <-" << std::endl;
//- std::cout << "Axis::get_axis_state timer before expiration <" << expiration_timer.time_to_expiration () << ">" << std::endl;
      //- request for moving active
      if (expiration_timer.time_to_expiration () > 0.)
      {
std::cout << "Axis::get_axis_state force MOVING state for <" <<  expiration_timer.time_to_expiration () << "> ms " << std::endl;

//-        this->force_state_update ();
        m_axis_state = AXIS_MOVING;
        return m_axis_state;
      }

      this->get_axis_state_i ();

      return m_axis_state;
    }
  }

  //-----------------------------------------------
  //- force_state_update
  //- force the PMACProxy Axis Status Update
  //-----------------------------------------------
  void Axis::force_state_update (void)
  {
    DEBUG_STREAM << "Axis::force_state_update <-" << std::endl;
    PMAC_PROXY->force_status_update ();
  }

  //-----------------------------------------------
  //- get_axis_status refresh the motor status
  //-----------------------------------------------
  void Axis::get_axis_status (std::string & status)
  {
    DEBUG_STREAM << "Axis::get_axis_status <-" << std::endl;
    {
      { //(- copy into critical section
        yat::AutoMutex <yat::Mutex> guard (this->m_status_lock);
        status.assign (m_axis_status);
      }
    }
  }

} //- namespace
