//=============================================================================
// PMACProxy.cpp
//=============================================================================
// abstraction.......PMAC communication class/thread
// class.............PMACProxy
// original author...J.Coquet d'apres projet ControlBox : N.Leclercq - SOLEIL
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/utils/XString.h>
#include "pmacinc/pcommx.h"
#include "StringTokenizer.h"


#include "PMACProxy.h"

// ============================================================================
// SOME CONSTs
// ============================================================================

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
#define kDEFAULT_CONTROLLER_ADDR "0.0.0.0"
#define kDEFAULT_CONTROLLER_PORT 1025
//-----------------------------------------------------------------------------

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kFORCE_STATUS_UPDATE (yat::FIRST_USER_MSG + 1000)
#define kSUSPEND             (yat::FIRST_USER_MSG + 1001)
#define kRESUME              (yat::FIRST_USER_MSG + 1002)
//-----------------------------------------------------------------------------

namespace pmac
{

// ============================================================================
// SINGLETON
// ============================================================================
PMACProxy * PMACProxy::singleton = 0;

// ============================================================================
// STATIC MEMBERS
// ============================================================================


// ============================================================================
// PMACProxy::Config::Config
// ============================================================================
PMACProxy::Config::Config ()
 : ip_addr (""),
	 port (0),
	 period_ms (500),
	 startup_timeout_ms (5000),
	 status_expiration_timeout_ms (1000)
{
	//- noop ctor
}

// ============================================================================
// PMACProxy::Config::operator=
// ============================================================================
void PMACProxy::Config::operator= (const Config& src)
{
	ip_addr = src.ip_addr;
	port = src.port;
	period_ms = src.period_ms;
	startup_timeout_ms = src.startup_timeout_ms;
	status_expiration_timeout_ms = src.status_expiration_timeout_ms;
}


// ============================================================================
// PMACProxy::init <STATIC MEMBER>
// ============================================================================
void PMACProxy::init ()
	throw (Tango::DevFailed)
{
	YAT_TRACE_STATIC("pmac::PMACProxy::init");

	//- already instanciated?
	if (PMACProxy::singleton)
		return;

	try
	{
		//- instanciate
		PMACProxy::singleton = new PMACProxy();
		if (PMACProxy::singleton == 0)
			throw std::bad_alloc();
	}
	catch (const std::bad_alloc&)
	{
		THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
										_CPTC("pmac::PMACProxy::singleton allocation failed"),
										_CPTC("pmac::PMACProxy::init"));
	}
	catch (Tango::DevFailed& df)
	{
		RETHROW_DEVFAILED(df,
											_CPTC("SOFTWARE_ERROR"),
											_CPTC("pmac::PMACProxy::singleton initialisation failed"),
											_CPTC("pmac::PMACProxy::init"));
	}
	catch (const yat::Exception& ye)
	{
	  _YAT_TO_TANGO_EXCEPTION(ye, te);
	  throw te;
	}
	catch (...)
	{
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
										_CPTC("pmac::PMACProxy::singleton initialisation failed [unknown exception]"),
										_CPTC("pmac::PMACProxy::init"));
	}
}

// ============================================================================
// PMACProxy::close  <STATIC MEMBER>
// ============================================================================
void PMACProxy::close ()
	throw (Tango::DevFailed)
{
	YAT_TRACE_STATIC("pmac::PMACProxy::close");

	try
	{
		//- check
		if (! PMACProxy::singleton)
			return;

		//- ask the underlying yat::Task to exit
		PMACProxy::singleton->exit();

		//- reset singleton
		PMACProxy::singleton = 0;

		//- NEVER CALL DELETE ON <PMACProxy::singleton>
		//- see yat::Task impl for more info...
	}
	_HANDLE_YAT_EXCEPTION("yat::Task::exit", "PMACProxy::close");
}

// ============================================================================
// PMACProxy::PMACProxy
// ============================================================================
PMACProxy::PMACProxy ()
	: m_cfg (),
    m_status_expired_error_counter (0),
		m_updated_once (false),
		m_suspended (false)
{
	YAT_TRACE("pmac::PMACProxy::PMACProxy");

	//- configure optional msg handling
	this->enable_timeout_msg(false);
	//- enable periodic msgs
	this->enable_periodic_msg(false);

}

// ============================================================================
// PMACProxy::~PMACProxy
// ============================================================================
PMACProxy::~PMACProxy ()
{
	YAT_TRACE("pmac::PMACProxy::~PMACProxy");
  //- close socket
	if (sockfd > 0)
    if (close_pmac_device (sockfd) == 0)
	      this->com_state = PMAC_SOCK_CLOSE;
}

// ============================================================================
// PMACProxy::configure
// ============================================================================
void PMACProxy::configure (const Config& _cfg)
	throw (Tango::DevFailed)
{
	YAT_TRACE("pmac::PMACProxy::configure");

  yat::Thread::State state;
  this->com_state = PMAC_UNKNOWN_ERROR;

  { //- enter critical section

	  yat::AutoMutex<yat::Mutex> guard(this->m_lock);

    //- get thread state under critical section
    state = this->state();

	  //- copy configuration
	  this->m_cfg = _cfg;

	  //- enable periodic msgs
	  this->enable_periodic_msg(true);
	  this->set_periodic_msg_period(this->m_cfg.period_ms);

	  //- be sure <m_cfg.status_expiration_tmo_ms> is set to a <correct> value
	  if (this->m_cfg.status_expiration_timeout_ms < 2 * this->m_cfg.period_ms)
		  this->m_cfg.status_expiration_timeout_ms = 2 * this->m_cfg.period_ms;

		//- (re)open connection to peer
	  try
	  {
	    if((sockfd = open_pmac_device (m_cfg.ip_addr.c_str ())) < 0)
        this->com_state = PMAC_SOCK_CREATE_ERROR;
      else
        this->com_state = PMAC_NO_ERROR;
	  }
	  catch (const std::bad_alloc&)
	  {
		  THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
										  _CPTC("failed to connect to the controller [comm instanciation failed]"),
										  _CPTC("pmac::PMACProxy::configure"));
	  }
	  catch (Tango::DevFailed& df)
	  {
		  RETHROW_DEVFAILED(df,
											  _CPTC("SOFTWARE_MEMORY"),
											  _CPTC("failed to connect to the controller [Tango exception caught]"),
											  _CPTC("pmac::PMACProxy::configure"));
	  }
	  catch (...)
	  {
		  THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
										  _CPTC("failed to connect to the controller [unknown exception caught]"),
										  _CPTC("pmac::PMACProxy::configure"));
	  }


	  //- init of the last full status update timestamp
	  _GET_TIME(this->m_last_update_ts);
  } //- end of critical section

	//- start the task if not running (must be done outside critical section)
	if (state != yat::Thread::STATE_RUNNING)
  {
    try
    {
      //- start the underlying task
		  this->go(_cfg.startup_timeout_ms);
		}
    _HANDLE_YAT_EXCEPTION("yat::Task::go","PMACProxy::configure");
  }
}


// ============================================================================
// PMACProxy::handle_message
// ============================================================================
void PMACProxy::handle_message (yat::Message& _msg)
	throw (yat::Exception)
{
	//- YAT_TRACE("pmac::PMACProxy::handle_message");

  //- enter critical section
  yat::AutoMutex<yat::Mutex> guard(this->m_lock);

	//- YAT_LOG("PMACProxy::handle_message::receiving msg " << _msg.to_string());

	//-static yat::Timestamp last, now;
	//-_GET_TIME(now);
	//-if (_IS_VALID_TIMESTAMP(last))
	//-	YAT_LOG("PMACProxy::handle_message::DT since last exec is " << _ELAPSED_MSEC(last, now) << " msecs");

  try
  {
	  //- handle msg
	  switch (_msg.type())
	  {
		  //- TASK_INIT ----------------------
		  case yat::TASK_INIT:
			  {
				  //- "initialization" code goes here
				  YAT_LOG("PMACProxy::handle_message::THREAD_INIT::PMACProxy-task is starting up");
          //- reserve enough space to avoid reallocation
          for (size_t i = 0; i < 8; i++)
            m_status[i].reserve (32);

          this->m_hw_poll_index = 0;
			  }
			  break;
		  //- TASK_EXIT ----------------------
		  case yat::TASK_EXIT:
			  {
				  //- "release" code goes here
				  YAT_LOG("PMACProxy::handle_message::THREAD_EXIT::PMACProxy-task is quitting");
			  }
			  break;
		  //- TASK_PERIODIC ------------------
		  case yat::TASK_PERIODIC:
			  {
			    //- TODO
          this->read_hard ();
          //- timestamp periodique
          yat::Timestamp ts;
          std::string date;
          _GET_TIME(ts);
          _TIMESTAMP_TO_DATE (ts, date);
          YAT_LOG( " PMACProxy::handle_message date = " << date << std::endl);
			  }
			  break;

			//- default ------------------------
		  default:
			  YAT_LOG("PMACProxy::handle_message::unhandled msg type received");
			  break;
	  }
	}
	catch (Tango::DevFailed& te)
	{
	  YAT_LOG("PMACProxy::handle_message::TANGO exception caught");
	  _TANGO_TO_YAT_EXCEPTION(te, ye);
#if defined (YAT_ENABLE_LOG)
	  ye.dump();
#endif
    //- GET_TIME(last);
	  throw ye;
	}
	catch (...)
	{
	  YAT_LOG("PMACProxy::handle_message::UNKNOWN exception caught");
	  //- _GET_TIME(last);
	  throw;
	}

  //-_GET_TIME(last);

	//- YAT_LOG("PMACProxy::handle_message::message_handler:msg " << _msg.to_string() << " successfully handled");
}


// ============================================================================
// PMACProxy::get
// ============================================================================
std::string PMACProxy::get (std::string cmd)
	throw (Tango::DevFailed)
{
  YAT_LOG("PMACProxy::get::<-");
  static char buff [256];
  ::memset (buff, 0, 256);
  { //- CRITICAL SECTION
//    yat::AutoMutex <yat::Mutex> guard (this->com_mutex);
    //- nb of chars if > 0 error code if < 0
    strncpy (buff,cmd.c_str (), cmd.length ());
    int i = pmac_get_response (sockfd, buff, 1024);
    if (i < 0)
    {
      switch (i)
      {
      case ERR_SOCKCREAT:
        com_state = PMAC_SOCK_CREATE_ERROR;
        break;
      case ERR_NOCONN:
        com_state = PMAC_NOCONNECT_ERROR;
        break;
      case ERR_SOCKCLOSE:
        com_state = PMAC_SOCK_CLOSE;
        break;
      case ERR_COMM1:
        com_state = PMAC_COM_ERROR_1;
        break;
      case ERR_COMM2:
        com_state = PMAC_COM_ERROR_2;
        break;
      case ERR_BADMSG:
        com_state = PMAC_MSG_ERROR;
        break;
      case ERR_BUFSIZE:
        com_state = PMAC_BUFFSIZE_ERROR;
        break;
      case ERR_WRITEBUF:
        com_state = PMAC_WRITE_ERROR;
        break;
      default :
        com_state = PMAC_UNKNOWN_ERROR;
        break;
      } //- end swich
      std::stringstream s;
      m_com_error ++;

      s << "Error trying communicate with PMAC cmd sent : ["
        << cmd
        << "] socket error : ["
        << com_state_str [com_state]
        << "]" << std::endl;

      YAT_LOG ("PMACProxy::get " << s.str () << std::endl);
		  THROW_DEVFAILED(_CPTC("COMMUNICATION_ERROR"),
									    s.str ().c_str (),
									    _CPTC("pmac::PMACProxy::get"));
    }
    else
    {
      m_com_success ++;
      com_state = PMAC_NO_ERROR;
      std::string resp (buff, i);
      YAT_LOG ("PMACProxy::get : cmd [" << cmd << "] returned [" << resp << std::endl);
      return resp;
    }
  }
}

// ============================================================================
// PMACProxy::set
// ============================================================================
std::string PMACProxy::set (std::string cmd)
	throw (Tango::DevFailed)
{
  YAT_LOG("PMACProxy::set::<-");
  static char buff [256];
  ::memset (buff, 0, 256);
  { //- CRITICAL SECTION
    yat::AutoMutex <yat::Mutex> guard (this->com_mutex);
    std::cout << "PMACProxy::set entering critical section" << std::endl;
    //- nb of chars if > 0 error code if < 0


    strcpy (buff,cmd.c_str ());
    int i = pmac_send_line (sockfd, buff);

    if (i < 0)
    {
      switch (i)
      {
      case ERR_SOCKCREAT:
        com_state = PMAC_SOCK_CREATE_ERROR;
        break;
      case ERR_NOCONN:
        com_state = PMAC_NOCONNECT_ERROR;
        break;
      case ERR_SOCKCLOSE:
        com_state = PMAC_SOCK_CLOSE;
        break;
      case ERR_COMM1:
        com_state = PMAC_COM_ERROR_1;
        break;
      case ERR_COMM2:
        com_state = PMAC_COM_ERROR_2;
        break;
      case ERR_BADMSG:
        com_state = PMAC_MSG_ERROR;
        break;
      case ERR_BUFSIZE:
        com_state = PMAC_BUFFSIZE_ERROR;
        break;
      case ERR_WRITEBUF:
        com_state = PMAC_WRITE_ERROR;
        break;
      default :
        com_state = PMAC_UNKNOWN_ERROR;
        break;
      } //- end swich
      std::stringstream s;
      m_com_error ++;

      s << "Error trying communicate with PMAC cmd sent : ["
        << cmd
        << "] socket error : ["
        << com_state_str [com_state]
        << "]" << std::endl;

      YAT_LOG ("PMACProxy::get " << s.str () << std::endl);
		  THROW_DEVFAILED(_CPTC("COMMUNICATION_ERROR"),
									    s.str ().c_str (),
									    _CPTC("pmac::PMACProxy::get"));
    }
    else
    {
      m_com_success ++;
      com_state = PMAC_NO_ERROR;
    }
  }
}

// ============================================================================
// PMACProxy::read_hard
// ============================================================================
void PMACProxy::read_hard (void)
{
  YAT_LOG("PMACProxy::read_hard::<-");

  //- index for hardware polling
  if (this->m_hw_poll_index > 2)
    this->m_hw_poll_index = 0;

  try
  {
    switch (this->m_hw_poll_index++)
    {
      case 0:               //- status for all axes
        YAT_LOG(" DEBUG : update status.......................");
        this->update_status ();
      break;
      case 1:              //- positions for all axes
        YAT_LOG(" DEBUG : update positions.......................");
        this->update_positions ();
      break;
      case 2:              //- velocity for all axes
        YAT_LOG(" DEBUG : update speeds.......................");
//        this->update_velocities ();
      break;
      default:
        m_hw_poll_index = 0;
    }
  }
  catch (Tango::DevFailed &e)
  {
    std::cerr << "PMACProxy::read_hard caught Tango::DevFailed [" << e.errors[0].desc << "]" << std::endl;
    ++m_com_error;
    ++m_consecutive_comm_errors;
    return;
  }
  catch (...)
  {
    std::cerr << "PMACProxy::read_hard caught (...)" << std::endl;
    ++m_com_error;
    ++m_consecutive_comm_errors;
    return;
  }
  m_consecutive_comm_errors = 0;
}


// ============================================================================
// PMACProxy::force_status_update
// ============================================================================
void PMACProxy::force_status_update (void)
{
  YAT_LOG("PMACProxy::force_status_update::<-");
  this->update_status ();
}


// ============================================================================
// PMACProxy::update_status
// ============================================================================
inline void PMACProxy::update_status (void)
{
  YAT_LOG("PMACProxy::update_status::<-");
  //- prepare <CTRL-B> binary cmd
  static unsigned char updt_st = 2;
  std::string response;
  std::string cmd (" ");
  cmd[0] = updt_st;

  //- do not handles exceptions, must be done by callers

  try
  {
yat::AutoMutex <yat::Mutex> guard (this->com_mutex);
    response = get (cmd);
    StringTokenizer tok (" ", response);
    if (tok.get_number_of_token () < 8)
    {
      //- TODO : set an error and set status
      std::cerr << "PMACProxy::update_status ERROR number of token = <" << tok.get_number_of_token () << ">" << std::endl;
      std::stringstream s;
      s << "invalid response to bin command <CTRL-B> (Update Status) got response ["
        << response
        << "]"
        << std::endl;
      THROW_DEVFAILED(_CPTC("COMMUNICATION_ERROR"),
                        s.str ().c_str (),
                        "PMACProxy::update_status");
    }
    { //- CRITICAL SECTION
      yat::AutoMutex <yat::Mutex> lock (status_mutex);

      m_status [0] = tok.get_token <std::string> (0);
      m_status [1] = tok.get_token <std::string> (1);
      m_status [2] = tok.get_token <std::string> (2);
      m_status [3] = tok.get_token <std::string> (3);
      m_status [4] = tok.get_token <std::string> (4);
      m_status [5] = tok.get_token <std::string> (5);
      m_status [6] = tok.get_token <std::string> (6);
      m_status [7] = tok.get_token <std::string> (7);
    } //- END CRITICAL SECTION
  }
  catch (Tango::DevFailed &e)
  {
    std::cerr << "PMACProxy::update_status invalid response ["
              << response
              << "] to status update"
              " caught DevFailed ["
              << e.errors[0].desc
              << std::endl;
  }
  catch (...)
  {
    std::cerr << "PMACProxy::update_status invalid response ["
              << response
              << "] to status update caught (...)"
              << std::endl;
  }

  return;
}
// ============================================================================
// PMACProxy::update_positions
// ============================================================================
inline void PMACProxy::update_positions (void)
{
  YAT_LOG("PMACProxy::update_positions::<-");
  //- prepare <CTRL-P> binary cmd
  static unsigned char updt_pos = 16;
  std::string response;
  std::string cmd (" ");
  cmd[0] = updt_pos;

  try
  {
yat::AutoMutex <yat::Mutex> guard (this->com_mutex);
    response = get (cmd);
//    std::cout << "PMACProxy::update_positions bin cmd <CTRL-P (Update Positions)> response [" << response << "]" << std::endl;
  }
  catch (Tango::DevFailed &e)
  {
    std::cout << "PMACProxy::update_positions ERROR trying to send bin cmd <CTRL-P (Update Positions)> caught Tango::DevFailed [" << e.errors[0].desc << "]" << std::endl;
    return;
  }
  catch (...)
  {
    std::cout << "PMACProxy::update_positions ERROR trying to send bin cmd <CTRL-P (Update Positions)> caught (...) " << std::endl;
    return;
  }

  try
  {
    StringTokenizer tok (" ", response);
    if (tok.get_number_of_token () < 8)
    {
      //- TODO : set an error and set status
      std::cout << "PMACProxy::update_positions ERROR number of token = <" << tok.get_number_of_token () << ">" << std::endl;
      std::stringstream s;
      s << "invalid response to bin command <CTRL-P> (Update Positions) got response ["
        << response
        << "]"
        << std::endl;
      THROW_DEVFAILED(_CPTC("COMMUNICATION_ERROR"),
                        s.str ().c_str (),
                        "PMACProxy::update_positions");
    }
    m_positions [0] = tok.get_token <double> (0);
    m_positions [1] = tok.get_token <double> (1);
    m_positions [2] = tok.get_token <double> (2);
    m_positions [3] = tok.get_token <double> (3);
    m_positions [4] = tok.get_token <double> (4);
    m_positions [5] = tok.get_token <double> (5);
    m_positions [6] = tok.get_token <double> (6);
    m_positions [7] = tok.get_token <double> (7);
    }
    catch (Tango::DevFailed &e)
    {
      std::cerr << "PMACProxy::update_positions invalid response ["
                << response
                << "] to positions update"
                " caught DevFailed ["
                << e.errors[0].desc
                << std::endl;
  }
  catch (...)
  {
    std::cerr << "PMACProxy::update_positions invalid response ["
              << response
              << "] to positions update caught (...)"
              << std::endl;
  }
  return;
}

// ============================================================================
// PMACProxy::update_velocities
// ============================================================================
inline void PMACProxy::update_velocities (void)
{
  YAT_LOG("PMACProxy::update_velocities::<-");
  //- prepare <CTRL-V> binary cmd
  static unsigned char updt_vel = 22;
  std::string response;
  std::string cmd (" ");
  cmd[0] = updt_vel;

  //- do not catch exception, must be done by clients
  response = get (cmd);
std::cout << "\t\tPMACProxy::update_velocities RESPONSE = " << response << std::endl;

  try
  {
    StringTokenizer tok (" ", response);
    if (tok.get_number_of_token () < 8)
    {
      //- TODO : set an error and set status
      std::cout << "PMACProxy::update_velocities ERROR number of token = <" << tok.get_number_of_token () << ">" << std::endl;
      std::stringstream s;
      s << "invalid response to bin command <CTRL-V> (Update Velocities) got response ["
        << response
        << "]"
        << std::endl;
      THROW_DEVFAILED(_CPTC("COMMUNICATION_ERROR"),
                        s.str ().c_str (),
                        "PMACProxy::update_velocities");
      return;
    }
    m_velocities [0] = tok.get_token <double> (0);
std::cout << "\t\tPMACProxy::update_velocities m_velocities [0] = " << m_velocities [0] << std::endl;
    m_velocities [1] = tok.get_token <double> (1);
    m_velocities [2] = tok.get_token <double> (2);
    m_velocities [3] = tok.get_token <double> (3);
    m_velocities [4] = tok.get_token <double> (4);
std::cout << "\t\tPMACProxy::update_velocities m_velocities [4] = " << m_velocities [4] << std::endl;
    m_velocities [5] = tok.get_token <double> (5);
    m_velocities [6] = tok.get_token <double> (6);
    m_velocities [7] = tok.get_token <double> (7);
  }
  catch (Tango::DevFailed &e)
  {
    std::cerr << "PMACProxy::update_velocities invalid response ["
              << response
              << "] to velocities update"
              " caught DevFailed ["
              << e.errors[0].desc
              << std::endl;
  }
  catch (...)
  {
    std::cerr << "PMACProxy::update_velocities invalid response ["
              << response
              << "] to velocities update caught (...)"
              << std::endl;
  }


  return;
}

// ============================================================================
// PMACProxy::check_valid
// ============================================================================
inline void PMACProxy::check_valid (size_t axis_number, std::string origin)
  throw (Tango::DevFailed)
{
  YAT_LOG("PMACProxy::check_valid::<-");

  std::string orig = "pmac::PMACProxy::" + origin;
  if (axis_number < 1 || axis_number > 8)
  {
    std::stringstream s;
    s << "axis_number <" << axis_number << "> out of range : [1...8]" << std::endl;
    YAT_LOG ("PMACProxy::axis_number " << s.str () << std::endl);
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      s.str ().c_str (),
                      orig.c_str ());
  }
  if (m_consecutive_comm_errors > 10)
  {
    std::stringstream s;
    s << "consecutive communication errors limit <10> exceeded " << std::endl;
    YAT_LOG ("PMACProxy::axis_number " << s.str () << std::endl);
      THROW_DEVFAILED(_CPTC("COMMUNICATION_ERROR"),
                      s.str ().c_str (),
                      orig.c_str ());
  }
  if (m_command_errors > 10)
  {
    std::stringstream s;
    s << "consecutive command errors limit <10> exceeded " << std::endl;
    YAT_LOG ("PMACProxy::axis_number " << s.str () << std::endl);
      THROW_DEVFAILED(_CPTC("COMMNAND_ERROR"),
                      s.str ().c_str (),
                      orig.c_str ());
  }
}

// ============================================================================
// PMACProxy::axis_position
// ============================================================================
double PMACProxy::axis_position (size_t axis_number)
    throw (Tango::DevFailed)
{
  YAT_LOG("PMACProxy::axis_position::<-");
  check_valid (axis_number, std::string ("axis_position"));
  return m_positions [axis_number -1];
}

// ============================================================================
// PMACProxy::axis_velocity
// ============================================================================
double PMACProxy::axis_velocity (size_t axis_number)
    throw (Tango::DevFailed)
{
  YAT_LOG("PMACProxy::axis_velocity::<-");

  check_valid (axis_number, std::string ("axis_velocity"));
 
    std::stringstream s;
    std::string resp;
    s << "i" << axis_number << "22";
yat::AutoMutex <yat::Mutex> guard (this->com_mutex);
      resp = this->get (s.str ());
      double val = yat::XString <double>::to_num (resp);

  return val;
}

// ============================================================================
// PMACProxy::axis_status
// ============================================================================
void PMACProxy::axis_status (size_t axis_number, std::string & resp)
    throw (Tango::DevFailed)
{
  YAT_LOG("PMACProxy::axis_status::<-");
  check_valid (axis_number, std::string ("axis_status"));
  {//- CRITICAL SECTION
    yat::AutoMutex <yat::Mutex> lock (status_mutex);
    resp.assign (m_status [axis_number -1]);
  }//- END CRITICAL SECTION
}


} // namespace pmac


/* 
Spyc (_all) [17]: a=soc.ExecLowLevelCmd('i122')

Spyc (_all) [18]: a
         Out[18]: '\x07ERR003'

*/


/*
Spyc (_all) [38]: a=soc.ExecLowLevelCmd('i122'); print a
521.7390 0 0 -1 0 0 0

*/



/*

Spyc (_all) [139]: a=soc.ExecLowLevelCmd('i122'); print a
ERR003
*/


/* 19-09-16
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 6.5
        vel = 6.5
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
PMACProxy::update_positions ERROR number of token = <1>
PMACProxy::update_positions invalid response [0] to positions update caught DevFailed [invalid response to bin command <CTRL-P> (Update Positions) got response [0
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
pmac_get_response: can't get response message from controller: Resource temporarily unavailable
PMACProxy::update_status invalid response [] to status update caught DevFailed [Error trying communicate with PMAC cmd sent : [] socket error : [Communication Error 2]

                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
                PMACProxy::axis_velocity:: vel = 6.5
        vel = 6.5
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725
        Axis::velocity <-
                PMACProxy::axis_velocity::<-
                PMACProxy::axis_velocity:: check valid DONE
                PMACProxy::axis_velocity:: vel = 99.8725
        vel = 99.8725


 19-09-16        */

